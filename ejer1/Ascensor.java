public boolean isValid() {
	if (this.pisoActual > 10 || this.pisoActual < 1 || this.pisoDestino > 10 || this.pisoDestino < 1) return false;
    if (this.enServicio) {
      if (this.subiendo == this.bajando) return false;
      if ((this.subiendo || this.bajando) && !this.puertaCerrada) return false;
      if (this.subiendo && this.pisoActual >= this.pisoDestino) return false;
      if (this.bajando && this.pisoActual <= this.pisoDestino) return false;
      return true;
    } else {
      if (!this.puertaCerrada || this.subiendo || this.bajando || this.pisoActual != this.pisoDestino) return false;
      return true;
    }	
}