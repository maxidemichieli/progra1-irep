public boolean isValid() {
	if (!this.tipoDeEmisora.equals("AM") || !this.tipoDeEmisora.equals("FM")) return false;
	if (!this.tipoDeEmisora.equals("AM") && (this.frecuencia > 1600 || this.frecuencia < 540)) return false;
	if (!this.tipoDeEmisora.equals("FM") && (this.frecuencia > 108 || this.frecuencia < 88)) return false;
	if (horarioInicio > horarioFin) return false;

	double presupuesto = 0;
	int rating = 70;
	for (int i = 0; i < this.periodistas.length; i++) {
		presupuesto += this.periodistas[i].sueldo;
		rating += this.periodistas[i].premios * 2;
	}

	if (this.presupuesto != presupuesto) return false;
	if (this.rating != rating) return false;

	return true;
}