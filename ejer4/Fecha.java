public boolean isValid() {
	if (this.mes < 1 || this.mes > 12) return false;
	if (this.mes < 1 || this.mes > 31) return false;
	if (mes == 2) {
		if (esBisiesto(this.anio)) {
			if (dia > 28) return false;
		} else {
			if (dia > 29) return false;
		}
	}
	int[] mesesCon31Dias = new int[] { 1, 3, 5, 7, 8, 10, 12};
	int[] mesesCon30Dias = new int[] { 4, 6, 9, 11};
	if (Arrays.asList(mesesCon31Dias).contains(this.mes) && this.dia > 31) return false;
	if (Arrays.asList(mesesCon30Dias).contains(this.mes) && this.dia > 30) return false;
	return true;
}