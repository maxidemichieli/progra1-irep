public boolean isValid() {
	if (nombre == null) return false;
	Date fechaHoy = new Date();
	int edad = fechaHoy.getYear() - this.fechaNacimiento.anio;
	if (this.fechaNacimiento.mes - fechaHoy.getMonth() >= 0 && this.fechaNacimiento.dia - fechaHoy.getDay() >= 0) edad -= 1;
	if (edad < 18) return false;
	if (this.sexo == 'M' && edad > 65) return false;
	if (this.sexo == 'F' && edad > 60) return false;
	return true;
}