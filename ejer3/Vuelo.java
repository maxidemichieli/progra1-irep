public boolean isValid() {
	double precioPrimeraClase = this.precioBase + (30 * this.precioBase / 100);
	if (this.primeraClase && this.precioFinal != precioPrimeraClase) return false;
	if (!this.primeraClase && this.precioFinal != this.precioBase) return false;
	if (!this.fechaHoraArribo.after(this.fechaHoraPartida)) return false;
	return true;
}