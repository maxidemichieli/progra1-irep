public boolean isValid() {
	String[] aeropuertos = new String[this.tramos.length * 2];
	boolean mismaAerolinea = true;
	Vuelo ultimoTramo = null;
	double precioTotal = 0;

	for (int i = 0; i < this.tramos.length; i += 2) {
		Vuelo tramoActual = this.tramos[i / 2];
		aeropuertos[i] = tramoActual.aeropuertoOrigen;
		aeropuertos[i+1] = tramoActual.aeropuertoDestino;
		precioTotal += tramoActual.precioFinal;
		if (ultimoTramo != null) {
		    if (!ultimoTramo.aeropuertoDestino.equals(tramoActual.aeropuertoOrigen)) return false;
		    if (ultimoTramo.fechaHoraArribo.horasDiferenciaCon(tramoActual.fechaHoraPartida) < 2) return false;
		    if (ultimoTramo.aerolinea != tramoActual.aerolinea) return false;
		}
		ultimoTramo = this.tramos[0];
	}

	if (mismaAerolinea) {
		if (this.costo != precioTotal - 7 * 10 / 100) return false;
	} else {
		if (this.costo != precioTotal) return false;			
	}

	for (int i = 0; i < aeropuertos.length; i++) {
		if (!Arrays.stream(this.aeropuertos).anyMatch(aeropuertos[i]::equals)) return false;
	}
	for (int i = 0; i < this.aeropuertos.length; i++) {
		if (!Arrays.stream(aeropuertos).anyMatch(this.aeropuertos[i]::equals)) return false;
	}

	return true;
}