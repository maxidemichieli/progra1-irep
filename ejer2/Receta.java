public boolean isValid() {
	if (this.cantidades.length != this.ingredientes.length) return false;
	boolean sinTaac = true;
	double costoTotal = 0;
	for (int i = 0; i < this.ingredientes.length; i++) {
		costoTotal += ingredientes[i].precioUnitario * cantidades[i];
		if (!ingredientes[i].sinTaac) sinTaac = false;
	}
	if (this.sinTaac != sinTaac) return false;
	if (this.costo != costoTotal) return false;
	double precioTotal;
	if (sinTaac) {
		precioTotal = costoTotal + (10 * costoTotal / 100);
	} else {
		precioTotal = costoTotal + (40 * costoTotal / 100);
	}
	if (this.precioVenta != precioTotal) return false;
	
	return true;
}